FROM openjdk:11-jre-slim
MAINTAINER puma
ADD /src/main/resources/application.properties application.properties
COPY target/project-0.0.1-SNAPSHOT.jar project-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/project-0.0.1-SNAPSHOT.jar"]